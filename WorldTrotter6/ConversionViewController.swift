//
//  ConversionViewController.swift
//  WorldTrotter6
//
//  Created by Connor Burk on 7/22/17.
//  Copyright © 2017 Connor Burk. All rights reserved.
//

import Foundation

import UIKit

class ConversionViewController: UIViewController, UITextFieldDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("test")
        // update celsius label upon launch
        updateCelsiusLabel()
    }
    
    // Label for celsius output
    @IBOutlet var celsiusLabel: UILabel!
    
    // fahrenheit variable
    // optional temperature measurement
    var fahrenheitValue: Measurement<UnitTemperature>? {
        didSet {
            updateCelsiusLabel()    // PROPERTY OBSERVER: code called when a property's
                                    //                    value changes
                                    // purpose (in this program): auto update celsius value
                                    //                            upon user input
        }
    }

    // computed property for celsius value
    var celsiusValue: Measurement<UnitTemperature>? {
        
        // check for fahrenheit value
        if let fahrenheitValue = fahrenheitValue {
            // convert fahrenheit value to celsius
            return fahrenheitValue.converted(to: .celsius)
        } else {
            return nil
        }
    }
    
    // Method: updateCelsiusLabel
    // Purpose: Update celsius label upon changed
    //          fahrenheit value
    func updateCelsiusLabel() {
        if let celsiusValue = celsiusValue {
            
            celsiusLabel.text = numberFormatter.string(from: NSNumber(value: celsiusValue.value))
        } else {
            celsiusLabel.text = "???"
        }
    }
    
    // outlet for text field 
    // purpose: dismiss keyboard
    @IBOutlet var textField: UITextField!
    
    
    // function: fahrenheitFieldEditingChanged
    // purpose:
    @IBAction func fahrenheitFieldEditingChanged(_ textField: UITextField) {
        
        // check for text
        // check for text's validity as double
        if let text = textField.text, let value = Double(text) {
            
            // Fahrenheit value set to init temp measurement
            fahrenheitValue = Measurement(value: value, unit: .fahrenheit)
        } else {
            fahrenheitValue = nil
        }
    }
    
    // function: dismissKeyboard
    // purpose: dismiss keyboard when user touches elsewhere besides decimal pad
    @IBAction func dismissKeyboard(_sender: UITapGestureRecognizer) {
        textField.resignFirstResponder()
    }
    
    
    // custom nuber formatter
    let numberFormatter: NumberFormatter = {
        
        // create an instance of a number formatter
        let nf = NumberFormatter()
        nf.numberStyle = .decimal
        nf.minimumFractionDigits = 0
        nf.maximumFractionDigits = 1
        return nf
    }()
    
    // working with a delegate and protocols 
    // UITextFieldDelegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("Current text: \(String(describing: textField.text))")
        print("Replacement text: \(string)")
        
        return true
    }

    

}
